using Godot;

namespace Kyniverse.Utils;

public static class Rect2Extension
{
    public static Rect2 Scale(this Rect2 _rect, float _scale) =>
        new Rect2(_rect.Position * _scale, _rect.Size * _scale);

    public static Rect2 Scale(this Rect2 _rect, Vector2 _scale) =>
        new Rect2(_rect.Position * _scale.X, _rect.Size * _scale.Y);

    public static Rect2 Scale(this Rect2 _rect, float _scaleX, float _scaleY) =>
        new Rect2(_rect.Position * _scaleX, _rect.Size * _scaleY);

    public static Rect2 Scale(this Rect2 _rect, Vector2 _scalePos, Vector2 _scaleSize) =>
        new Rect2(_rect.Position * _scalePos, _rect.Size * _scaleSize);

    public static Rect2 Translate(this Rect2 _rect, Vector2 _relative) =>
        new Rect2(_rect.Position + _relative, _rect.Size);
}