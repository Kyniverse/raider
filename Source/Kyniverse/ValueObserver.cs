namespace Kyniverse.Utils;

using System;

public class ValueObersver<T>
{
//SECTION FIELDS
    private T _value;
//!SECTION

//SECTION PROPERTIES
    public T Value
    {
        get => _value;
        set
        {
            if (!_value.Equals(value))
            {
                _value = value;
                Changed?.Invoke();
            }
        }
    }
//!SECTION

//SECTION EVENTS
    public Action Changed;
//!SECTION
}