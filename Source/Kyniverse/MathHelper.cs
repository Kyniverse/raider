namespace Kyniverse.Utils;

using Godot;
using System;

public static class Util
{
	public static bool Approx(float _value, float _target, float _bias) =>
		(Mathf.Abs(_value - _target) < _bias);

	public static bool Approx(Vector2 _value, Vector2 _target, Vector2 _bias) =>
		(_value - _target).Abs() < _bias;

	public static float Map(float _input, float _inputMin, float _inputMax, float _outputMin, float _outputMax) =>
		(_outputMin + (_input - _inputMin) * (_outputMax - _outputMin) / (_inputMax - _inputMin));

	public static float Fract(float _x) => 
		(_x - Mathf.Floor(_x));

	public static Vector2 Fract(Vector2 _value) => 
		new Vector2(Fract(_value.X), Fract(_value.Y));

	public static Vector3 Fract(Vector3 _value) => 
		new Vector3(Fract(_value.X), Fract(_value.Y), Fract(_value.Z));

	public static float Random1D(float _value) => 
		Fract(Mathf.Sin(_value) * 43758.5453f);
		
	public static Vector2 Random2D(Vector2 _position) => Fract(new Vector2(
		Mathf.Sin(_position.Dot(new Vector2(127.1f, 311.7f))),
		Mathf.Sin(_position.Dot(new Vector2(296.5f, 183.3f)))) * 43758.5453f
	);
}