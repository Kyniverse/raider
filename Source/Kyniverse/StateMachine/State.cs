namespace Kyniverse.Systems;

using Godot;
using System;

internal abstract partial class State : Godot.GodotObject
{
//SECTION METHODS
	public virtual void Enter() {; }
	public virtual void Process(double delta) {; }
	public virtual void PhysicsProcess(double delta) {; }
	public virtual void GuiInputEvent(InputEvent @event) {; }
	public virtual void Exit() {; }
//!SECTION
}