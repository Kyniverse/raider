namespace Kyniverse.Systems;

using Godot;
using System;
using System.Collections.Generic;

internal sealed partial class StateMachine : Godot.GodotObject
{
//SECTION FIELDS
	private State currentState;
	private State nextState;
	private State previousState;
	private Dictionary<byte, State> states = new Dictionary<byte, State>();
//!SECTION

//SECTION CONSTRUCTORS
	public StateMachine() {; }
//!SECTION

//SECTION PROPERTIES
	public State CurrentState { get => currentState; }
	public State NextState { get => nextState; }
	public State PreviousState { get => previousState; }
//!SECTION

//SECTION SIGNALS
	[Signal] public delegate void OnStateChangedEventHandler(State _state);
	[Signal] public delegate void OnStateProcessEventHandler(State _state);
	[Signal] public delegate void OnStatePhysicsProcessEventHandler(State _state);
	[Signal] public delegate void OnGuiInputEventEventHandler(State _state, InputEvent _event);
	[Signal] public delegate void OnStateExitedEventHandler(State _state);
//!SECTION

//SECTION METHODS
	public void Register(State state) => states.Add((Byte)states.Count, state);
	public void Switch(Byte newState) => ChangeState(states[newState]);
	public void Switch(State newState) => ChangeState(newState);

	public void Process(double delta)
	{
		if (currentState is null) return;
		currentState.Process(delta);
		EmitSignal(nameof(OnStateProcess), currentState);
	}

	public void PhysicsProcess(float delta)
	{
		if (currentState is null) return;
		currentState.PhysicsProcess(delta);
		EmitSignal(nameof(OnStatePhysicsProcess), currentState);
	}

	public void GuiInputEvent(InputEvent @event)
	{
		if (currentState is null) return;
		currentState.GuiInputEvent(@event);
		EmitSignal(nameof(OnGuiInputEvent), currentState, @event);
	}

	private void ChangeState(State newState)
	{
		nextState = newState;

		if (currentState is not null)
		{
			currentState.Exit();
			EmitSignal(nameof(OnStateExited), currentState);
			previousState = currentState;
		}

		currentState = newState;
		currentState.Enter();

		EmitSignal(nameof(OnStateChanged), currentState);

		nextState = null;
	}
//!SECTION
}