public static class Vector3Extension
{
//  SECTION PROPERTIES
    public static Vector3 PlaneXY => new (1.0f, 1.0f, 0.0f);
    public static Vector3 PlaneXZ => new (1.0f, 0.0f, 1.0f);
    public static Vector3 PlaneYX => new (0.0f, 1.0f, 1.0f);
//  !SECTION

//  SECTION METHODS
    public static Vector3 GetXZ(this Vector3 vector) => new Vector3(vector.X, 0.0f, vector.Z);

    public static Vector3 SetX(this Vector3 vector, float x) => new (x, vector.Y, vector.Z);
    public static Vector3 SetY(this Vector3 vector, float y) => new (vector.X, y, vector.Z);
    public static Vector3 SetZ(this Vector3 vector, float z) => new (vector.X, vector.Y, z);

    // public static Vector3 SetXY(this Vector3 vector, float x, float y) => new (x, y, vector.Z);
    // public static Vector3 SetXZ(this Vector3 vector, float x, float z) => new (x, y, vector.Z);
    // public static Vector3 SetYZ(this Vector3 vector, float y, float z) => new (x, y, vector.Z);
//  !SECTION
}