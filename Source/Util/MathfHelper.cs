using Godot;

internal sealed class MathfHelper
{
//  SECTION Constants
    /// <summary>
    /// Prime number used for Random hashing
    /// </summary>
    public const float Prime1 = 127.983f;

    /// <summary>
    /// Prime number used for Random hashing
    /// </summary>
    public const float Prime2 = 293.541f;

    /// <summary>
    /// Prime number used for Random hashing
    /// </summary>
    public const float Prime3 = 311.751f;

    /// <summary>
    /// Prime number used for Random hashing
    /// </summary>
    public const float Prime4 = 439.241f;

    /// <summary>
    /// Prime number used for Random hashing
    /// </summary>
    public const float Prime5 = 43758.5453f;

    /// <summary>
    /// Pi Over (Divided by) 2
    /// </summary>
	public const float Pi2 = Mathf.Pi / 2f;

    /// <summary>
    /// Pi Over (Divided by) 3
    /// </summary>
	public const float Pi3 = Mathf.Pi / 3f;

    /// <summary>
    /// Pi Over (Divided by) 4
    /// </summary>
	public const float Pi4 = Mathf.Pi / 4f;

    /// <summary>
    /// Pi Over (Divided by) 5
    /// </summary>
	public const float Pi5 = Mathf.Pi / 5f;

    /// <summary>
    /// Pi Over (Divided by) 6
    /// </summary>
	public const float Pi6 = Mathf.Pi / 6f;

    /// <summary>
    /// Pi Over (Divided by) 7
    /// </summary>
	public const float Pi7 = Mathf.Pi / 7f;

    /// <summary>
    /// Pi Over (Divided by) 8
    /// </summary>
	public const float Pi8 = Mathf.Pi / 8f;

    /// <summary>
    /// Number of radians in a degree
    /// </summary>
    public const float RadiansPerDegree = Mathf.Pi / 180.0f;

    /// <summary>
    /// Checks if a floating-point value is approximately equal to a target value within a specified bias.
    /// </summary>
    /// <param name="value">The value to be compared.</param>
    /// <param name="target">The target value for the approximation.</param>
    /// <param name="bias">The allowable difference (bias) for the approximation.</param>
    /// <returns>True if the values are approximately equal; otherwise, false.</returns>
    public static bool Approx(float value, float target, float bias) => 
        Mathf.Abs(value - target) < bias;

    /// <summary>
    /// Checks if a vector-2 value is approximately equal to a target value within a specified bias.
    /// </summary>
    /// <param name="value">The value to be compared.</param>
    /// <param name="target">The target value for the approximation.</param>
    /// <param name="bias">The allowable difference (bias) for the approximation.</param>
    /// <returns>True if the values are approximately equal; otherwise, false.</returns>
    public static bool Approx(Vector2 value, Vector2 target, Vector2 bias) => 
        (value - target).Abs() < bias;

    /// <summary>
    /// Checks if a vector-3 value is approximately equal to a target value within a specified bias.
    /// </summary>
    /// <param name="value">The value to be compared.</param>
    /// <param name="target">The target value for the approximation.</param>
    /// <param name="bias">The allowable difference (bias) for the approximation.</param>
    /// <returns>True if the values are approximately equal; otherwise, false.</returns>
    public static bool Approx(Vector3 value, Vector3 target, Vector3 bias) => 
        (value - target).Abs() < bias;

    /// <summary>
    /// Maps a value from one range to another, with optional clamping.
    /// </summary>
    /// <param name="value">The input value to be mapped.</param>
    /// <param name="inputMin">The minimum value of the input range.</param>
    /// <param name="inputMax">The maximum value of the input range.</param>
    /// <param name="outputMin">The minimum value of the output range.</param>
    /// <param name="outputMax">The maximum value of the output range.</param>
    /// <param name="clamp">Optional. If true, clamps the result to the output range; otherwise, allows values outside the range.</param>
    /// <returns>The mapped value within the specified output range.</returns>
    public static float Map(float value, float inputMin, float inputMax, float outputMin, float outputMax, bool clamp = true)
    {
        var _value = outputMin + ((value - inputMin) * (outputMax - outputMin) / (inputMax - inputMin));

        if (clamp)
        {
            _value = outputMax > outputMin
                ? Mathf.Clamp(_value, outputMin, outputMax)
                : Mathf.Clamp(_value, outputMax, outputMin);
        }

        return _value;
    }

    /// <summary>
    /// Computes the fractional part of a floating-point number.
    /// </summary>
    /// <param name="x">The input value.</param>
    /// <returns>The fractional part of the input value.</returns>
    public static float Fract(float x) => x - Mathf.Floor(x);

    /// <summary>
    /// Computes the fractional parts of each component in a 2D vector.
    /// </summary>
    /// <param name="value">The input 2D vector.</param>
    /// <returns>A new Vector2 containing the fractional parts of the input vector's X and Y components.</returns>
    public static Vector2 Fract(Vector2 value) => new (
        Fract(value.X), 
        Fract(value.Y));

    /// <summary>
    /// Computes the fractional parts of each component in a 3D vector.
    /// </summary>
    /// <param name="value">The input 3D vector.</param>
    /// <returns>A new Vector3 containing the fractional parts of the input vector's X, Y, and Z components.</returns>
    public static Vector3 Fract(Vector3 value) => new (
        Fract(value.X), 
        Fract(value.Y), 
        Fract(value.Z));

    /// <summary>
    /// Generates a pseudo-random 1D value based on the sine of time
    /// </summary>
    /// <returns>A pseudo-random 1D value between 0 and 1.</returns>
    public static float Random1D() => (GD.Randf() * 2.0f) - 1.0f;

    /// <summary>
    /// Generates a pseudo-random 1D value based on the sine of the input value.
    /// </summary>
    /// <param name="value">The input value used to calculate the sine-based randomness.</param>
    /// <returns>A pseudo-random 1D value between 0 and 1.</returns>
    public static float Random1D(float value) => Fract(Mathf.Sin(value) * Prime5);

    /// <summary>
    /// Generates a pseudo-random 2D vector with components in the range [-1, 1].
    /// </summary>
    /// <returns>A Vector2 with random X and Y components between -1 and 1.</returns>
    public static Vector2 Random2D() => new (
        (GD.Randf() * 2.0f) - 1.0f, 
        (GD.Randf() * 2.0f) - 1.0f);

    /// <summary>
    /// Generates a pseudo-random 2D vector based on the input position.
    /// </summary>
    /// <param name="_position">The input position used to calculate the pseudo-random 2D vector.</param>
    /// <returns>A Vector2 with components generated using a pseudo-random algorithm.</returns>
    public static Vector2 Random2D(Vector2 _position) => Fract(new Vector2(
        Mathf.Sin(_position.Dot(new Vector2(Prime1, Prime3))), 
        Mathf.Sin(_position.Dot(new Vector2(Prime2, Prime4)))) * Prime5);

    /// <summary>
    /// Generates a pseudo-random 3D vector with components in the range [-1, 1].
    /// </summary>
    /// <returns>A Vector3 with random X, Y, and Z components between -1 and 1.</returns>
    public static Vector3 Random3D() => new (
        (GD.Randf() * 2.0f) - 1.0f, 
        (GD.Randf() * 2.0f) - 1.0f, 
        (GD.Randf() * 2.0f) - 1.0f);

    /// <summary>
    /// Generates a pseudo-random 3D vector based on the input position.
    /// </summary>
    /// <param name="_position">The input position used to calculate the pseudo-random 3D vector.</param>
    /// <returns>A Vector2 with components generated using a pseudo-random algorithm.</returns>
    public static Vector3 Random3D(Vector3 _position) => Fract(new Vector3(
        Mathf.Sin(_position.Dot(new Vector3(Prime1, Prime3, Prime2))),
        Mathf.Sin(_position.Dot(new Vector3(Prime2, Prime4, Prime1))),
        Mathf.Sin(_position.Dot(new Vector3(Prime3, Prime1, Prime4)))) * Prime5);
//  !SECTION
}
