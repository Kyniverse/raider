using System;

namespace Raider.Entities;



internal sealed partial class OrbitalCamera : Node3D
{
//  SECTION CONSTANTS
    private const float MinAllowedCameraDistance = 0.0f;
    private const float MaxAllowedCameraDistance = 64.0f;
    private const float MinAllowedPitchRadians = -MathfHelper.Pi2;
    private const float MaxAllowedPitchRadians = MathfHelper.Pi2;
//  !SECTION

//  SECTION FIELDS
    private float _distance = Mathf.Pi;
    private float _angleYaw;
    private float _anglePitch;
    
    private float _minDistance = Mathf.Pi;
    private float _maxDistance = Mathf.Tau;
    private float _minPitchRadians = MinAllowedPitchRadians;
    private float _maxPitchRadians = MaxAllowedPitchRadians;

    [ExportGroup("References")]
    [Export] private Camera3D _cameraNode;
//  !SECTION

//  SECTION PROPERTIES
    public float Yaw
    {
        get => _angleYaw;
        set => _angleYaw = Mathf.Wrap(value, 0.0f, Mathf.Tau);
    }

    public float Pitch
    {
        get => _anglePitch;
        set => _anglePitch = Mathf.Clamp(value, MinPitchRadians, MaxPitchRadians);
    }

    public Camera3D Camera 
    { 
        get => _cameraNode; 
    }

    [ExportGroup("Limits")]
    [Export] public float Distance 
    {
        get => _distance;
        set => _distance = Mathf.Clamp(value, _minDistance, _maxDistance);
    }

    [Export] public float MinDistance
    {
        get => _minDistance;
        set => Mathf.Clamp(value, MinAllowedCameraDistance, MaxAllowedCameraDistance);
    }

    [Export] public float MinPitchRadians
    {
        get => _minPitchRadians;
        set => _minPitchRadians = Mathf.Clamp(value, MinAllowedPitchRadians, MaxAllowedPitchRadians);
    }

    [Export] public float MaxPitchRadians
    {
        get => _maxPitchRadians;
        set => _maxDistance = Mathf.Clamp(value, MinAllowedPitchRadians, MaxAllowedPitchRadians);
    }
//  !SECTION

//  SECTION METHODS
    public sealed override void _Ready()
    {
        base._Ready();
    }

    public sealed override void _Process(double delta)
    {
        if (_cameraNode is null) return;
        UpdateCameraPosition();
        base._Process(delta);
    }

    private void UpdateCameraPosition()
    {
        float targetX = Mathf.Sin(Yaw);
        float targetY = Mathf.Sin(-Pitch);
        float targetZ = Mathf.Cos(Yaw);
        float pitchDistanceMultiplier = Mathf.Cos(Pitch);

        _cameraNode.Position = new Vector3(
            targetX * pitchDistanceMultiplier, targetY, 
            targetZ * pitchDistanceMultiplier) * Distance;

        _cameraNode.Rotation = new Vector3(Pitch, Yaw, 0.0f);
    }
//  !SECTION
}