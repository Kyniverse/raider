namespace Raider.Entities.PlayerStates;



internal sealed partial class Jumping : PlayerState
{
    Vector3 forwardVector = Vector3.Zero;
    float gravity = ProjectSettings.GetSetting("physics/3d/default_gravity").AsSingle();

    public Jumping(PlayerController player) : base(player) {; }

    public sealed override void Enter()
    {
        forwardVector = Player.Velocity.GetXZ().LengthSquared() > 0.1f
            ? Player.Velocity.GetXZ().Normalized() * Player.JumpDistance
            : Vector3.Zero;

        Player.Velocity = Vector3.Up * Player.JumpDistance;
    }

    public sealed override void PhysicsProcess(double delta)
    {
        Player.Velocity = forwardVector.SetY(Player.Velocity.Y);
        Player.Velocity += Vector3.Down * gravity * (float)delta;

        if (Player.Velocity.Y < 0)
            Player.StateMachine.Switch((byte)PlayerController.States.FALLING);
    }
}