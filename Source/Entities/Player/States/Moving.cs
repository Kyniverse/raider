namespace Raider.Entities.PlayerStates;

using System;
using Kyniverse.Utils;

internal sealed partial class Moving : PlayerState
{
//SECTION CONSTRUCTORS
    public Moving(PlayerController player) : base(player) {; }

    public sealed override void PhysicsProcess(double delta)
    {
        if (!Player.IsMoving) 
            Player.StateMachine.Switch((byte)PlayerController.States.IDLE);

        if (Input.IsActionJustPressed("PLAYER_JUMP")) 
            Player.StateMachine.Switch((byte)PlayerController.States.JUMPING);
            
        Player.UpdateMovement(delta);
    }
//!SECTION
}