namespace Raider.Entities;

using Kyniverse.Systems;

internal abstract partial class PlayerState : State
{
//SECTION PROPERTIES
    public PlayerController Player { get; init; }
//!SECTION

//SECTION CONSTRUCTORS
    public PlayerState(PlayerController player) => 
        Player = player;
//!SECTION
}