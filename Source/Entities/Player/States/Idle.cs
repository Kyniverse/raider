namespace Raider.Entities.PlayerStates;

using Godot;
using System;
using Kyniverse.Utils;

internal sealed partial class Idle : PlayerState
{
//SECTION CONSTRUCTORS
    public Idle(PlayerController player) : base(player) {; }

    public sealed override void PhysicsProcess(double delta)
    {
        if (Player.IsMoving) 
            Player.StateMachine.Switch((byte)PlayerController.States.MOVING);

        if (Input.IsActionJustPressed(PlayerController.ActionPlayerJump))
            Player.StateMachine.Switch((byte)PlayerController.States.JUMPING);

        Player.UpdateMovement(delta);
    }
//!SECTION
}