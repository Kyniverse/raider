namespace Raider.Entities.PlayerStates;

using System;
using Godot;

internal sealed partial class Falling : PlayerState
{
    float gravity = ProjectSettings.GetSetting("physics/3d/default_gravity").AsSingle();

    public Falling(PlayerController player) : base(player) {; }

    public sealed override void PhysicsProcess(double delta)
    {
        if (!Player.IsOnFloor) Player.Velocity += Vector3.Down * gravity * (float)delta;
        else Player.StateMachine.Switch((byte)PlayerController.States.IDLE);
    }
}