namespace Raider.Entities;

using Godot;
using Kyniverse.Utils;
using Kyniverse.Systems;

internal sealed partial class PlayerController : Node
{
//	SECTION CONSTANTS
	public const string ActionPlayerForward = "PLAYER_FORWARD";
	public const string ActionPlayerBackward = "PLAYER_BACKWARD";
	public const string ActionPlayerLeft = "PLAYER_LEFT";
	public const string ActionPlayerRight = "PLAYER_RIGHT";
	public const string ActionPlayerJump = "PLAYER_JUMP";
	public const string ActionPlayerWalk = "PLAYER_WALK";

	public const string ActionCameraUp = "CAMERA_UP";
	public const string ActionCameraDown = "CAMERA_DOWN";
	public const string ActionCameraLeft = "CAMERA_LEFT";
	public const string ActionCameraRight = "CAMERA_RIGHT";
//	!SECTION

//	SECTION FIELDS
	[ExportGroup("References")]
	[Export] private Node3D _model;
	[Export] private CharacterBody3D _characterBody;
	[Export] private OrbitalCamera _orbitalCamera;
	[Export] private AnimationTree _animationTree;
//	!SECTION

//	SECTION ENUMS
	internal enum States : byte
	{
		IDLE			=	0,
		MOVING			=	1,
		JUMPING			=	2,
		FALLING			=	3
	}
//	!SECTION

//	SECTION PROPERTIES
	public StateMachine StateMachine { get; } = new StateMachine();
	public Vector3 Velocity { get => _characterBody.Velocity; set => _characterBody.Velocity = value; }
	public bool IsMoving { get => !Util.Approx(_characterBody.Velocity.LengthSquared(), 0.0f, .01f); }
	public bool IsOnFloor { get => _characterBody.IsOnFloor(); }
	public static Vector2 CameraInputVector { get => Input.GetVector(ActionCameraLeft, ActionCameraRight, ActionCameraUp, ActionCameraDown); }
	public static Vector2 MovementInputVector { get => Input.GetVector(ActionPlayerLeft, ActionPlayerRight, ActionPlayerForward, ActionPlayerBackward); }



	[ExportGroup("Movement")]
	[Export] public float MoveSpeed { get; set; } = 1f;
	[Export] public float MovementAcceleration { get; set; } = Mathf.Tau;
	[Export] public float JumpDistance { get; set; } = 2.0f;
//	!SECTION

//	SECTION METHODS
	public override void _Ready()
	{
		StateMachine.Register(new PlayerStates.Idle(this));
		StateMachine.Register(new PlayerStates.Moving(this));
		StateMachine.Register(new PlayerStates.Jumping(this));
		StateMachine.Register(new PlayerStates.Falling(this));
		StateMachine.Switch((byte)States.IDLE);

		StateMachine.OnStateChanged += (state) => GD.Print(state.GetType().Name);
	}

	public override void _Process(double delta)
	{
		StateMachine.Process(delta);

		UpdateCamera(delta);
		UpdatePlayerMesh(delta);

		base._Process(delta);
	}

	public override void _PhysicsProcess(double delta)
	{
		StateMachine.PhysicsProcess((float)delta);
		_characterBody.MoveAndSlide();

		base._PhysicsProcess(delta);
	}

	internal void UpdateMovement(double delta)
	{
		Vector3 inputVector3 = new (MovementInputVector.X, 0.0f, MovementInputVector.Y);
		float targetSpeed = MovementInputVector.LengthSquared() > 0
			? Input.IsActionPressed(ActionPlayerWalk) ? MoveSpeed * 0.5f : MoveSpeed
			: 0.0f;

		inputVector3 = inputVector3.Rotated(Vector3.Up, _orbitalCamera.Yaw);
		
		Velocity = Velocity.Lerp(inputVector3 * targetSpeed, (float)delta * MovementAcceleration);
		Velocity = MathfHelper.Approx(Velocity.GetXZ(), Vector3.Zero, Vector3.One * 0.001f)
			? Vector3.Zero.SetY(Velocity.Y) : Velocity;
	}

	private void UpdateCamera(double delta)
	{
		_orbitalCamera.Yaw += CameraInputVector.X * (float)delta;
		_orbitalCamera.Pitch += CameraInputVector.Y * (float)delta;
	}

	private void UpdatePlayerMesh(double delta)
	{
		float lerpAngle = Mathf.LerpAngle(
			_model.Rotation.Y, 
			MathfHelper.Pi2 + Mathf.Atan2(Velocity.Z, -Velocity.X),
			(float)delta * Mathf.Tau
		);

		_model.Rotation = new Vector3(0.0f, lerpAngle,0.0f);
		_animationTree.Set("parameters/WalkToRun/blend_position", _characterBody.Velocity.LengthSquared() * 0.25f);
	}

//	!SECTION
}